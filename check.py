import json
import datetime
import threading
from time import sleep
from copy import deepcopy

#import requests
import schedule
import paho.mqtt.client    as mqtt
from pushover import Client



# "Objects" are things like a car or your cell phone. 



class Checker():
    def __init__(self):
        
        self.config_path  = '/etc/weather_mqtt/config.json'
        self.config       = self.load_config()
        self.locations    = {}
        self.alerts_cache = {}
        self.client       = mqtt.Client()
        self.n            = noaa.NOAA()


        self.client.username_pw_set(username=self.config['mqtt_username'], password=self.config['mqtt_password'])
        self.client.tls_set()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.client.connect(self.config['mqtt_host'], self.config['mqtt_port'])

        now = datetime.datetime.utcnow()

        for topic in self.config['object_topics']:
            self.alerts_cache[topic] = []
            schedule.every(self.config['update_interval']).seconds.do(self.check_object, topic)


        self.worker_thread = threading.Thread(target=self.schedule_loop)
        self.worker_thread.start()


        # this blocks
        self.client.loop_forever()
        

    def on_connect(self, client, userdata, flags, rc):
        print('connected with result code: %s' % (rc))

        for topic in self.config['object_topics']:
            self.client.subscribe(topic)


    def on_message(self, client, userdata, msg):
        try:
            j = json.loads(msg.payload.decode('utf-8'))

        except Exception as e:
            print('error decoding JSON: %s' % (e))
            return 

        print('topic: %s received: %s' % (msg.topic, msg.payload))



        if 'lat' not in j or 'lon' not in j:
            print('latitude or longitude is not in the JSON data')
            return 

        if msg.topic not in self.config['object_topics']:
            print("topic not in self.config['object_topics']")
            return 

        self.locations[msg.topic] = {'lat':j['lat'], 'lon':j['lon']} 

        print('location updated for: %s' % (msg.topic))


   
    def notify_by_pushbullet(self, alert, urgent=False):
        client = Client(self.config['pushover_user_key'], api_token=self.config['pushover_api_token'])

      
        if urgent:
            client.send_message(alert['message'], title=alert['long'], priority=2, expire=2000, retry=30, sound='siren')


    def check_object(self, object_topic):
        print('Checking %s' % (object_topic))

        # update the alerts

        if object_topic not in self.locations.keys():
            print('unable to find object in self.locations')
            return
       
        alerts = self.get_alerts(self.locations[object_topic])

        for alert in alerts:
            if alert not in self.alerts_cache[object_topic]:
                
                # send out the alert
                if object_topic not in self.config['notify_method']:
                    print('There is no notifcation method set for: %s' % (object_topic))


                elif self.config['notify_method'][object_topic] == 'pushbullet':
                    print('sending out alert via pushbullet')

                    if alert['short'] in self.config['pushover_urgent']:
                        self.notify_by_pushbullet(alert, urgent=True)

                    else:
                        self.notify_by_pushbullet(alert)


        # update the cache
        self.alerts_cache[object_topic] = alerts


        print('alerts for %s: %s' % (object_topic, alerts))







    def load_config(self):
        with open(self.config_path, 'r') as cfg:
            return json.load(cfg)


    def get_page(self, url):
        page = requests.get(url)

        if page.status_code != 200:
            print('unable to get page: %s' % (page.url))


        try:
            return page.json()

        except Exception as e:
            print('unable to decode page: %s' (page.url))
        
        return page.json()


    def get_alerts(self, location):

        alerts = []
        hail   = False


        #d = self.get_page('http://api.wunderground.com/api/%s/alerts/q/%s,%s.json' % (self.config['wg_api_key'], location['lat'], location['lon']))

        
        #d = d['alerts']

        #print(d)


       
        self.n.alerts(point='%s,%s' % (location['lat'], location['lon']), active=1)

        d = alerts['features']

        

        for x in range(len(d)):
            alert = deepcopy(d[x]['properties'])

            #if alert['description'] in self.config['alerts_ignore']:
            #    continue 

            #alerts.append({'short':alert['type'], 'long':alert['description'], 'message':alert['message']})  

            

            for word in alert['description'].split(' '):
                if hail == False:
                    hail = True
                    #alerts.append({'short':'HAL', 'long':'hail', 'message':'hail detected near you'})



        #for alert in alerts:
        #    self.publisher.publish(msg=alert, title='weather_alert')

        return alerts



    def schedule_loop(self):
        while True:
            schedule.run_pending()
            sleep(1)


if __name__ == '__main__':
    checker = Checker()

    
